FROM nginx:latest
USER root
RUN apt update && apt install htop -y
WORKDIR /usr/share/nginx/html/
COPY ./index.html /usr/share/nginx/html/
EXPOSE 80
